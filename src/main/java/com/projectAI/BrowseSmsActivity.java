package com.projectAI;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.projectAI.services.SmsReceiver;
public class BrowseSmsActivity extends Activity implements OnClickListener, OnItemClickListener {

    ArrayList<String> smsList = new ArrayList<String>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(android.R.style.Theme_Light);
        setContentView(R.layout.browse_sms);

        this.findViewById(R.id.UpdateList).setOnClickListener(this);
    }

    public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
        try {
            String[] splitted = smsList.get(pos).split("\n");
            String sender = splitted[0];
            String encryptedData = "";
            for (int i = 1; i < splitted.length; ++i) {
                encryptedData += splitted[i];
            }
            decryptionDialog(encryptedData, this);
        } catch (Exception e) {
            Toast.makeText(this, "Decryption error " + e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);

        int indexBody = cursor.getColumnIndex(SmsReceiver.BODY);
        int indexAddress = cursor.getColumnIndex(SmsReceiver.ADDRESS);
        if (indexBody < 0 || !cursor.moveToFirst()) return;

        smsList.clear();

        do {
            String str = "Sender: " + cursor.getString(indexAddress) + "\n" + cursor.getString(indexBody) + "\n";
            smsList.add(str);
        } while (cursor.moveToNext());

        ListView smsListView = (ListView) findViewById(R.id.SMSList);
        smsListView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, smsList));
        smsListView.setOnItemClickListener(this);
    }

    protected void decryptionDialog(final String encMsg, final Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Salt");
        alert.setMessage("Enter Pass-phrase");

        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                try {
                    String data = StringCryptor.decrypt(encMsg, value);
                    displayDialog(data);
                } catch (Exception e) {
                    Toast.makeText(context, "Decryption failed.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                return;
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        alert.show();
    }

    protected void displayDialog(final String decMsg) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Decrypted Message");
        alert.setMessage(decMsg);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return;
            }
        });
        alert.show();
    }

}