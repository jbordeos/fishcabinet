package com.projectAI.helper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHelper {

	public Date getCurrentDate() {
		return Calendar.getInstance().getTime();
	}
	
	public String formatToDisplayDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
		return dateFormat.format(date);
	}
	
	public String formatToDBDate(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		return dateFormat.format(date);
	}
}
