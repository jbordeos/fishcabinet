package com.projectAI;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.SecureRandom;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by IntelliJ IDEA.
 * User: joel
 * Date: 9/5/11
 * Time: 1:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringCryptor {
    /*private static final String CIPHER_ALGORITHM = "AES";
	private static final String RANDOM_GENERATOR_ALGORITHM = "ODELEINE";
	private static final int RANDOM_KEY_SIZE = 128;

	// Encrypts string and encode in Base64
	public static String encrypt( String password, String data ) throws Exception
	{
		byte[] secretKey = generateKey( password.getBytes() );
	    byte[] clear = data.getBytes();

	    SecretKeySpec secretKeySpec = new SecretKeySpec( secretKey, CIPHER_ALGORITHM );
		Cipher cipher = Cipher.getInstance( CIPHER_ALGORITHM );
	    cipher.init( Cipher.ENCRYPT_MODE, secretKeySpec );

	    byte[] encrypted = cipher.doFinal( clear );
	    String encryptedString = Base64.encodeToString( encrypted, Base64.DEFAULT );

		return encryptedString;
	}

	// Decrypts string encoded in Base64
	public static String decrypt( String password, String encryptedData ) throws Exception
	{
		byte[] secretKey = generateKey( password.getBytes() );

		SecretKeySpec secretKeySpec = new SecretKeySpec( secretKey, CIPHER_ALGORITHM );
		Cipher cipher = Cipher.getInstance( CIPHER_ALGORITHM );
	    cipher.init( Cipher.DECRYPT_MODE, secretKeySpec );

	    byte[] encrypted = Base64.decode( encryptedData, Base64.DEFAULT );
	    byte[] decrypted = cipher.doFinal( encrypted );

		return new String( decrypted );
	}

	public static byte[] generateKey( byte[] seed ) throws Exception
	{
		KeyGenerator keyGenerator = KeyGenerator.getInstance(CIPHER_ALGORITHM);
		SecureRandom secureRandom = SecureRandom.getInstance(RANDOM_GENERATOR_ALGORITHM);
		secureRandom.setSeed( seed );
	    keyGenerator.init( RANDOM_KEY_SIZE, secureRandom );
	    SecretKey secretKey = keyGenerator.generateKey();
	    return secretKey.getEncoded();
	}*/
     private static final String ALGORITHM = "AES";
    private static final int ITERATIONS = 2;
    private static final byte[] SECRET_KEY = new byte[] {'t','h','i','s','i','s','a','s','e','c','r','e','t','k','e','y'};

     public static String encrypt(String value, String salt) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);

        String valueToEnc = null;
        String eValue = value;
        for (int i = 0; i < ITERATIONS; i++) {
            valueToEnc = salt + eValue;
            byte[] encValue = c.doFinal(valueToEnc.getBytes());
            eValue = com.projectAI.Base64.encodeBytes(encValue);
            Log.d("SimpleSMSEncDecAPplcation-----------", "Encrypted: " + eValue);
        }
        return eValue;
    }
     public static String decrypt(String value, String salt) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);

        String dValue = null;
        String valueToDecrypt = value;
        for (int i = 0; i < ITERATIONS; i++) {
            byte[] decodedValue = com.projectAI.Base64.decode(valueToDecrypt); //new BASE64Decoder().decodeBuffer(valueToDecrypt);
            byte[] decValue = c.doFinal(decodedValue);

            dValue = new String(decValue).substring(salt.length());
            valueToDecrypt = dValue;
            Log.d("SimpleSMSEncDecApplication-----------", "Decrypted: " + dValue);
        }
        return dValue;
    }

    private static Key generateKey() throws Exception {
        Key key = new SecretKeySpec(SECRET_KEY, ALGORITHM);
        // SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
        // key = keyFactory.generateSecret(new DESKeySpec(keyValue));
        return key;
    }
}
