package com.projectAI.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.ancore.service.GenericEntityServiceImpl;
import com.projectAI.models.Customer;
import com.projectAI.screens.InboxScreen;

public class SmsReceiver extends BroadcastReceiver {
	/** TAG used for Debug-Logging */
	private static final String LOG_TAG = "SmsReceiver";
	public static final String SMS_EXTRA_NAME = "pdus";
	public static final String SMS_URI = "content://sms";

	public static final String ADDRESS = "address";
	public static final String PERSON = "person";
	public static final String DATE = "date";
	public static final String READ = "read";
	public static final String STATUS = "status";
	public static final String TYPE = "type";
	public static final String BODY = "body";
	public static final String SEEN = "seen";

	public static final int MESSAGE_TYPE_INBOX = 1;
	public static final int MESSAGE_TYPE_SENT = 2;

	public static final int MESSAGE_IS_NOT_READ = 0;
	public static final int MESSAGE_IS_READ = 1;

	public static final int MESSAGE_IS_NOT_SEEN = 0;
	public static final int MESSAGE_IS_SEEN = 1;

	private ArrayList<Customer> custList;
	private Map<String, Customer> custMap;
	private GenericEntityService genericEntityService;

	/**
	 * The Action fired by the Android-System when a SMS was received. We are
	 * using the Default Package-Visibility
	 */
	private static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(ACTION)) {
			// if(message starts with SMStretcher recognize BYTE)
			String messages = "";
			String address = "";
			/* The SMS-Messages are 'hiding' within the extras of the Intent. */
			Bundle extras = intent.getExtras();
			if (extras != null) {
				// Get received SMS array
				Object[] smsExtra = (Object[]) extras.get(SMS_EXTRA_NAME);

				for (int i = 0; i < smsExtra.length; ++i) {
					SmsMessage sms = SmsMessage
							.createFromPdu((byte[]) smsExtra[i]);

					String body = sms.getMessageBody().toString();
					address = sms.getOriginatingAddress();

					messages += "SMS from " + address + " :\n";
					messages += body + "\n";
				}

				// Display SMS message
				// Toast.makeText(context, messages, Toast.LENGTH_SHORT).show();
			}
			/* Logger Debug-Output */
			Log.i(LOG_TAG, "[SMSApp] onReceiveIntent: " + messages);

			/* Show the Notification containing the Message. */
			// Toast.makeText(context, messages, Toast.LENGTH_LONG).show();

			address = address.replaceAll("\\+63", "0");
			genericEntityService = new GenericEntityServiceImpl(context.getApplicationContext());
			custList = genericEntityService.findAll(Customer.class);
			if (custList != null) {
				custMap = new HashMap<String, Customer>();
				for (int i = 0; i < custList.size(); i++) {
					custMap.put(custList.get(i).contactNo, custList.get(i));
				}
			}
			Log.i(LOG_TAG,
					context.getPackageName() + "\n" + context.databaseList());
			if (custMap != null && custMap.containsKey(address)) {
				/* Start the Main-Activity */
				Intent i = new Intent(context, InboxScreen.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
			}
		}
	}
}
