package com.projectAI;

import android.app.Application;

import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.ancore.service.GenericEntityServiceImpl;

public class FishCabinetApplication extends Application {

	public GenericEntityService genericEntityService;
	public static boolean isDebuggable = false;

	public GenericEntityService getGenericEntityService() {
		if (genericEntityService == null)
			genericEntityService = new GenericEntityServiceImpl(this);

		return genericEntityService;
	}
	
}
