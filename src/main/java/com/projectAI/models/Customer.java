package com.projectAI.models;

import com.projectAI.ancore.model.LongPkEntity;

public class Customer extends LongPkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String name;
	public String address;
	public String contactNo;

}
