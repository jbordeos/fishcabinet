package com.projectAI.models;


import com.projectAI.ancore.model.LongPkEntity;

public class SentItem extends LongPkEntity {

	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	public String name;
	public String number;
	public String message;
	public String date;
	public boolean sent;
	
	public SentItem(String name, String number, String message,
			String date, boolean sent) {
		this.name = name;
		this.number = number;
		this.message = message;
		this.date = date;
		this.sent = sent;
	}
	
	public SentItem() {
	}
}
