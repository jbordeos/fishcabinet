package com.projectAI.models;

import com.projectAI.ancore.model.LongPkEntity;

public class Fish extends LongPkEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6692991072693564552L;
	@SampleAnnotation(fieldType = "String")
	public String name;
	public double bPrice;
	public double sPrice;
	public int qty;
	public double kilo;
	public String dishes;
}
