package com.projectAI.models;

import com.projectAI.ancore.model.LongPkEntity;

public class TodaysFish extends LongPkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String date;
	public Long fishId;
	public String fishName;
	public double bPrice;
	public double sPrice;
	public double kilo;

	public TodaysFish() {

	}

	public TodaysFish(String date, Long fishId, String fishName,
			double fishBPrice, double fishSPrice, double kilo) {
		this.date = date;
		this.fishId = fishId;
		this.fishName = fishName;
		this.bPrice = fishBPrice;
		this.sPrice = fishSPrice;
		this.kilo = kilo;
	}
	
}
