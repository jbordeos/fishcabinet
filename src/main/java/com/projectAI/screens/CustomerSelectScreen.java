package com.projectAI.screens;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projectAI.FishCabinetApplication;
import com.projectAI.R;
import com.projectAI.ancore.domain.BaseActivity;
import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.helper.DateHelper;
import com.projectAI.models.Customer;
import com.projectAI.models.SentItem;
import com.projectAI.models.TodaysFish;
import com.projectAI.screens.TodaysFishCabinetScreen.FishModel;
import com.projectAI.screens.TodaysFishCabinetScreen.InteractiveArrayAdapter;
import com.projectAI.screens.TodaysFishCabinetScreen.ViewHolder;

public class CustomerSelectScreen extends BaseActivity {

	private GenericEntityService genericEntityService;
	private ArrayList<CustomerSelect> list;
	private Button btnSend;
	private ListView lvList;
	private boolean alreadySend = false;
	private boolean sentItemFlag = false;
	private DateHelper dateHelper;
	private int ctr = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customer_select);
	}

	private class CustomerSelect {
		private Customer cust;
		private boolean selected;

		public CustomerSelect(Customer cust) {
			this.cust = cust;
			selected = false;
		}
	}

	private void initComponent() {
		btnSend = (Button) findViewById(R.id.btnSend);
		lvList = (ListView) findViewById(R.id.lvCustomers);

		OnClickListener listener = new OnClickListener() {
			private ViewHolder viewHolder;

			@Override
			public void onClick(View v) {
				viewHolder = (ViewHolder) v.getTag();
				CustomerSelect cs = (CustomerSelect) viewHolder.checkbox
						.getTag();
				cs.selected = !cs.selected;
				viewHolder.checkbox.setChecked(cs.selected);
				list.get(viewHolder.position).selected = cs.selected;
			}
		};

		ArrayAdapter<CustomerSelect> adapter = new CustomerSelectArrayAdapter(
				this, R.layout.customer_select_row, list, listener);
		lvList.setAdapter(adapter);

		btnSend = (Button) findViewById(R.id.btnSend);
		btnSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String query = "select * from "
						+ TodaysFish.class.getSimpleName()
						+ " where date = '"
						+ dateHelper.formatToDBDate(dateHelper.getCurrentDate())
						+ "'";
				ArrayList<TodaysFish> list = genericEntityService
						.sampleCustomQuery(query, TodaysFish.class);
				StringBuilder msg = new StringBuilder("");
				DecimalFormat dFormat = new DecimalFormat("0.00");
				if (list != null) {
					for (int i = 0; i < list.size(); i++) {
						msg.append(list.get(i).fishName + " (P "
								+ dFormat.format(list.get(i).sPrice) + ")");
						if (i != list.size() - 1) {
							msg.append(", ");
						}
					}
				}
				callSendDialog(msg.toString());
			}
		});
	}

	private void callSendDialog(String msg) {
		final Dialog dialog = new Dialog(getContext());
		StringBuilder recipients = new StringBuilder();
		for (CustomerSelect cs : list) {
			if (cs.selected) {
				recipients.append(cs.cust.name + "(" + cs.cust.contactNo
						+ ")\n");
			}
		}
		dialog.setTitle("Recipients: " + recipients.toString());
		dialog.setContentView(R.layout.reply_dialog);
		final EditText etMsg = (EditText) dialog.findViewById(R.id.etMsg);
		etMsg.setText(msg);
		Button btnSend = (Button) dialog.findViewById(R.id.btnSend);
		Button btnClear = (Button) dialog.findViewById(R.id.btnClear);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				etMsg.setText("");
			}
		});
		btnSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (etMsg.getText().toString().length() > 0) {
					for (CustomerSelect cs : list) {
						SmsManager sms = SmsManager.getDefault();
						ArrayList<String> parts = sms.divideMessage(etMsg
								.getText().toString());
						for (int i = 0; i < parts.size(); i++) {
							sendSMS(cs.cust, etMsg
									.getText().toString(),
									"(" + (i + 1) + "/" + parts.size()
											+ ") \n" + parts.get(i));
						}
					}
					dialog.dismiss();
				} else {
					Toast.makeText(v.getContext(), "Please enter your message",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	static class ViewHolder {
		protected TextView text;
		protected CheckBox checkbox;
		protected int position;
	}

	private class CustomerSelectArrayAdapter extends
			ArrayAdapter<CustomerSelect> {

		private final List<CustomerSelect> list;
		private final Activity context;
		private final int rowLayout;
		private OnClickListener listener;

		public CustomerSelectArrayAdapter(Activity context, int rowLayout,
				List<CustomerSelect> list, OnClickListener listener) {
			super(context, rowLayout, list);
			this.list = list;
			this.listener = listener;
			this.context = context;
			this.rowLayout = rowLayout;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;
			if (convertView == null) {
				LayoutInflater inflator = context.getLayoutInflater();
				view = inflator.inflate(rowLayout, null);
				final ViewHolder viewHolder = new ViewHolder();
				viewHolder.text = (TextView) view.findViewById(R.id.tvName);
				viewHolder.checkbox = (CheckBox) view.findViewById(R.id.check);
				viewHolder.position = position;
				view.setTag(viewHolder);
				viewHolder.checkbox.setTag(list.get(position));
			} else {
				view = convertView;
				((ViewHolder) view.getTag()).checkbox
						.setTag(list.get(position));
				((ViewHolder) view.getTag()).position = position;
			}
			if (this.listener != null) {
				view.setOnClickListener(this.listener);
			}
			Log.d("getView>>>", "position: " + position);
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.text.setText(list.get(position).cust.name);
			holder.checkbox.setChecked(list.get(position).selected);
			return view;
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void doInBackgroundProcess() {
		dateHelper = new DateHelper();
		genericEntityService = ((FishCabinetApplication) getApplication())
				.getGenericEntityService();
		ArrayList<Customer> custList = genericEntityService
				.findAll(Customer.class);
		list = new ArrayList<CustomerSelectScreen.CustomerSelect>();
		if (custList != null) {
			for (Customer cust : custList) {
				list.add(new CustomerSelect(cust));
			}
		}
	}

	@Override
	protected void onPostExecuteProcess() {
		initComponent();
	}

	private void saveToSentItems(Customer customer, String message, boolean sent) {
		SentItem si = new SentItem(customer.name, customer.contactNo, message,
				new Date().toString(), sent);
		genericEntityService.create(si);
	}

	private void sendSMS(final Customer cust, final String message, final String messagePart) {
		showProgressDialog("Please wait...", this);
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";
		Intent sentIntent = new Intent(SENT);
		sentIntent.putExtra("name", cust.name);
		sentIntent.putExtra("number", cust.contactNo);

		PendingIntent sentPI = PendingIntent.getBroadcast(this, ctr++,
				sentIntent, 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(this, ctr++,
				new Intent(DELIVERED), 0);
		if (!alreadySend) {
			// ---when the SMS has been sent---
			registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent intent) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS Sent",
								Toast.LENGTH_SHORT).show();
						/*
						 * Log.d("INBOXSCREEN", "SMS Sent " +
						 * intent.getExtras().getString("name") + " : " +
						 * intent.getExtras().getString("number"));
						 */
						if (!sentItemFlag) {
							saveToSentItems(cust, message, true);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getContext());
						builder.setTitle("You have no load").setNegativeButton(
								"OK", null);
						AlertDialog alert = builder.create();
						alert.show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						Toast.makeText(getBaseContext(), "No service",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						Toast.makeText(getBaseContext(), "Null PDU",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						Toast.makeText(getBaseContext(), "Radio off",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					}
				}
			}, new IntentFilter(SENT));

			// ---when the SMS has been delivered---
			registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS delivered",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS delivered");
						break;
					case Activity.RESULT_CANCELED:
						Toast.makeText(getBaseContext(), "SMS not delivered",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS not delivered");
						break;
					}
				}
			}, new IntentFilter(DELIVERED));
		}

		SmsManager sms = SmsManager.getDefault();
		ArrayList<String> parts = sms.divideMessage(messagePart);
		try {
			sms.sendTextMessage(cust.contactNo, null, parts.get(0), sentPI,
					deliveredPI);
			Log.i("AIiiiiiiiiiiiiiiiiiiiiii", cust.contactNo + "\n" + message
					+ "\n" + sentPI + "\n" + deliveredPI);
			alreadySend = true;
		} catch (Exception e) {
			Toast.makeText(this, "error" + e.getMessage(), Toast.LENGTH_LONG)
					.show();
		}
	}
}
