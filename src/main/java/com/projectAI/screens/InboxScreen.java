package com.projectAI.screens;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.projectAI.FishCabinetApplication;
import com.projectAI.R;
import com.projectAI.ancore.domain.BaseActivity;
import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.helper.DateHelper;
import com.projectAI.models.Customer;
import com.projectAI.models.SentItem;
import com.projectAI.models.TodaysFish;
import com.projectAI.services.SmsReceiver;

public class InboxScreen extends BaseActivity implements OnItemClickListener {
	private ListView smsListView;
	private ArrayList<HashMap<String, String>> inboxMap;
	private ArrayList<Customer> custList;
	private Map<String, Customer> custMap;
	private GenericEntityService genericEntityService;
	private boolean alreadySend = false;
	private boolean sentItemFlag = false;
	private DateHelper dateHelper;
	private Spinner spnCustomers;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTheme(android.R.style.Theme_Light);
		setContentView(R.layout.inbox);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	private void updateInboxList(Map<String, Customer> custMap) {
		ContentResolver cr = getContentResolver();
		Cursor cursor = cr.query(Uri.parse("content://sms/inbox"), null, null,
				null, null);

		int indexID = cursor.getColumnIndex("_id");
		int indexBody = cursor.getColumnIndex(SmsReceiver.BODY);
		int indexDate = cursor.getColumnIndex(SmsReceiver.DATE);
		int indexAddress = cursor.getColumnIndex(SmsReceiver.ADDRESS);
		int indexStatus = cursor.getColumnIndex(SmsReceiver.READ);
		if (indexBody < 0 || !cursor.moveToFirst())
			return;
		String[] from = new String[] { "sender", "date", "status", "body" };
		int[] to = new int[] { R.id.tvSender, R.id.tvDate, R.id.tvStatus,
				R.id.tvBody };
		inboxMap = new ArrayList<HashMap<String, String>>();
		do {
			String id = cursor.getString(indexID);
			String sender = cursor.getString(indexAddress).replaceAll("\\+63",
					"0");
			String msg = cursor.getString(indexBody);
			String strDate = cursor.getString(indexDate);
			strDate = new SimpleDateFormat("MMM dd, yyyy HH:mm")
					.format(new Date(Long.parseLong(strDate)));
			String read = cursor.getString(indexStatus);
			if (read.equals("0")) {
				read = "[UNREAD]";
			} else {
				read = "";
			}
			if (custMap != null) {
				if (custMap.containsKey(sender)) {
					// for adapter
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("id", id);
					map.put("sender", custMap.get(sender).name);
					map.put("senderNo", sender);
					map.put("date", strDate);
					map.put("status", read);
					map.put("body", msg);
					inboxMap.add(map);
				}
			}
		} while (cursor.moveToNext());

		SimpleAdapter adapter = new SimpleAdapter(this, inboxMap,
				R.layout.inbox_row, from, to);
		smsListView.setAdapter(adapter);
		smsListView.setOnItemClickListener(this);
	}

	private void initComponent() {
		smsListView = (ListView) findViewById(R.id.lvInbox);
		updateInboxList(custMap);
		spnCustomers = (Spinner) findViewById(R.id.spnCustomers);
		ArrayList<String> contacts = new ArrayList<String>();
		contacts.add("All Contacts");
		if (custList != null) {

			for (int i = 0; i < custList.size(); i++) {
				contacts.add(custList.get(i).name);
			}
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, contacts);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnCustomers.setAdapter(adapter);
		spnCustomers.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> av, View v, int position,
					long id) {
				custMap = new HashMap<String, Customer>();
				if (position != 0) {
					if (custList != null) {
						custMap.put(custList.get(position - 1).contactNo,
								custList.get(position - 1));
					}
				} else {
					if (custList != null) {
						for (int i = 0; i < custList.size(); i++) {
							custMap.put(custList.get(i).contactNo,
									custList.get(i));
						}
					}
				}
				updateInboxList(custMap);
			}

			@Override
			public void onNothingSelected(AdapterView<?> av) {

			}

		});
	}

	private void callReplyDialog(String msg, final Customer cust) {
		final Dialog dialog = new Dialog(getContext());
		dialog.setTitle("Reply to " + cust.name + " (" + cust.contactNo + ")");
		dialog.setContentView(R.layout.reply_dialog);
		final EditText etMsg = (EditText) dialog.findViewById(R.id.etMsg);
		etMsg.setText(msg);
		Button btnSend = (Button) dialog.findViewById(R.id.btnSend);
		Button btnClear = (Button) dialog.findViewById(R.id.btnClear);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				etMsg.setText("");
			}
		});
		btnSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (etMsg.getText().toString().length() > 0) {
					SmsManager sms = SmsManager.getDefault();
					ArrayList<String> parts = sms.divideMessage(etMsg.getText()
							.toString());
					for (int i = 0; i < parts.size(); i++) {
						sendSMS(cust, etMsg.getText()
								.toString(), "(" + (i + 1) + "/" + parts.size()
								+ ") \n" + parts.get(i));
					}
					dialog.dismiss();
				} else {
					Toast.makeText(v.getContext(), "Please enter your message",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int index, long arg3) {

		// update read status of message clicked
		ContentValues values = new ContentValues();
		values.put("read", true);
		getContentResolver().update(Uri.parse("content://sms/"), values,
				"_id=" + inboxMap.get(index).get("id"), null);

		((TextView) v.findViewById(R.id.tvStatus)).setText("");
		// end
		String query = "select * from " + TodaysFish.class.getSimpleName()
				+ " where date = '"
				+ dateHelper.formatToDBDate(dateHelper.getCurrentDate()) + "'";
		ArrayList<TodaysFish> list = genericEntityService.sampleCustomQuery(
				query, TodaysFish.class);
		StringBuilder msg = new StringBuilder("");
		DecimalFormat dFormat = new DecimalFormat("0.00");
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				msg.append(list.get(i).fishName + " (P "
						+ dFormat.format(list.get(i).sPrice) + ")");
				if (i != list.size() - 1) {
					msg.append(", ");
				}
			}
		}
		Customer customer = custMap.get(inboxMap.get(index).get("senderNo"));
		callReplyDialog(msg.toString(), customer);
	}

	@Override
	protected void doInBackgroundProcess() {
		dateHelper = new DateHelper();
		genericEntityService = ((FishCabinetApplication) getApplication())
				.getGenericEntityService();
		custList = genericEntityService.findAll(Customer.class);
		if (custList != null) {
			custMap = new HashMap<String, Customer>();
			for (int i = 0; i < custList.size(); i++) {
				custMap.put(custList.get(i).contactNo, custList.get(i));
			}
		}
	}

	@Override
	protected void onPostExecuteProcess() {
		initComponent();
	}

	private void saveToSentItems(Customer customer, String message, boolean sent) {
		SentItem si = new SentItem(customer.name, customer.contactNo, message,
				new Date().toString(), sent);
		genericEntityService.create(si);
	}

	private void sendSMS(final Customer cust, final String message, final String messagePart) {
		showProgressDialog("Please wait...", this);
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";

		final PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
				new Intent(SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 1,
				new Intent(DELIVERED), 0);
		if (!alreadySend) {
			// ---when the SMS has been sent---
			registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent intent) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS Sent",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS Sent");
						if (!sentItemFlag) {
							saveToSentItems(cust, message, true);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getContext());
						builder.setTitle("You have no load").setNegativeButton(
								"OK", null);
						AlertDialog alert = builder.create();
						alert.show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						Toast.makeText(getBaseContext(), "No service",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						Toast.makeText(getBaseContext(), "Null PDU",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						Toast.makeText(getBaseContext(), "Radio off",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							saveToSentItems(cust, message, false);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					}
				}
			}, new IntentFilter(SENT));

			// ---when the SMS has been delivered---
			registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS delivered",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS delivered");
						break;
					case Activity.RESULT_CANCELED:
						Toast.makeText(getBaseContext(), "SMS not delivered",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS not delivered");
						break;
					}
				}
			}, new IntentFilter(DELIVERED));
		}

		SmsManager sms = SmsManager.getDefault();
		ArrayList<String> parts = sms.divideMessage(messagePart);
		try {
			/*
			 * sms.sendMultipartTextMessage(cust.contactNo, null, parts,
			 * sentIntents, deliveryIntents);
			 */
			sms.sendTextMessage(cust.contactNo, null, parts.get(0), sentPI,
					deliveredPI);
			Log.i("AIiiiiiiiiiiiiiiiiiiiiii", cust.contactNo + "\n" + message
					+ "\n" + sentPI + "\n" + deliveredPI);
			alreadySend = true;
		} catch (Exception e) {
			Toast.makeText(this, "error" + e, Toast.LENGTH_LONG).show();
		}
	}

}
