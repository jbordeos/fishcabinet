package com.projectAI.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projectAI.FishCabinetApplication;
import com.projectAI.R;
import com.projectAI.models.Fish;

import com.projectAI.ancore.domain.BaseActivity;
import com.projectAI.ancore.service.GenericListAdapter;
import com.projectAI.ancore.service.GenericEntityService;

public class FishDirectoryScreen extends BaseActivity {
	String D = "FISHDIRECTORY";
	public OnTouchListener gestureListener;
	private ListView llFishDirectory;
	private Button btnAddFish;
	private final int[] colors = { Color.WHITE, Color.CYAN };
	private GenericEntityService genericEntityService;
	private ArrayList<Fish> fishList;
	private GenericListAdapter<Fish> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fish_directory);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void doInBackgroundProcess() {
		genericEntityService = ((FishCabinetApplication) getApplication())
				.getGenericEntityService();
	}

	@Override
	public void onPostExecuteProcess() {
		initComponent();
	}

	private void deleteDialog(final View v, final int pos) {
		String name = ((TextView) v.findViewById(R.id.tvName)).getText()
				.toString();
		AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
		builder.setIcon(android.R.drawable.ic_delete)
				.setTitle("DELETE " + name)
				.setMessage("Are you sure you?")
				.setPositiveButton("DELETE",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								if (genericEntityService.delete(fishList
										.get(pos))) {
									fishList.remove(pos);
									updateFishListView();
								} else {
									Toast.makeText(v.getContext(), "Failed",
											Toast.LENGTH_LONG).show();
								}
							}
						}).setNegativeButton("CANCEL", null);
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void updateFishListView() {
		String query = "select * from Fish order by name asc";
		fishList = genericEntityService.sampleCustomQuery(query, Fish.class);
		if (fishList != null) {
			gestureListener = new View.OnTouchListener() {
				private int padding = 0;
				private int initialx = 0;
				private int currentx = 0;
				private ViewHolder viewHolder;
				private int color = Color.parseColor("#ffffff");
				private int colorRed = Color.parseColor("#F9966B");
				private int colorWhite = Color.parseColor("#ffffff");
				private int img = R.drawable.bullet_go;
				private int imgArrow = R.drawable.greenfish_right;
				private int imgWarn = R.drawable.redfish_delete;

				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						Log.v(D, "Action DOWN");
						padding = 0;
						initialx = (int) event.getX();
						currentx = (int) event.getX();
						viewHolder = ((ViewHolder) v.getTag());
					}
					if (event.getAction() == MotionEvent.ACTION_MOVE) {
						currentx = (int) event.getX();
						padding = currentx - initialx;
						Log.v(D, "Action MOVE : padding = " + padding);
					}

					if (event.getAction() == MotionEvent.ACTION_UP
							|| event.getAction() == MotionEvent.ACTION_CANCEL) {
						Log.v(D, "Action UP or CANCEL");
						if (padding > 100) {
							deleteDialog(v, viewHolder.position);
						}
						padding = 0;
						initialx = 0;
						currentx = 0;
						color = colorWhite;
						img = imgArrow;
					}

					if (viewHolder != null) {
						if (padding >= 100) {
							Log.v(D, "Padding > 75");
							color = colorRed;
							img = imgWarn;
						}
						if (padding < 100) {
							color = colorWhite;
							img = imgArrow;
						}
						v.setBackgroundColor(color);
						viewHolder.icon.setImageResource(img);
						v.setPadding(padding, 0, 0, 0);
					}
					return true;
				}
			};

			FishArrayAdapter adapter = new FishArrayAdapter(this, fishList,
					gestureListener);
			llFishDirectory.setAdapter(adapter);
		} else {
			FishArrayAdapter adapter = new FishArrayAdapter(this,
					new ArrayList<Fish>(), gestureListener);
			llFishDirectory.setAdapter(adapter);
		}
	}

	private void initComponent() {
		llFishDirectory = (ListView) findViewById(R.id.lvFishDirectory);
		updateFishListView();

		btnAddFish = (Button) findViewById(R.id.btnAddFish);
		btnAddFish.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final Dialog dialog = new Dialog(v.getContext());
				dialog.setTitle("Add Fish");
				dialog.setContentView(R.layout.add_fish_dialog);
				final EditText etName = (EditText) dialog
						.findViewById(R.id.etName);
				Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
				Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
				btnSave.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Fish fish = new Fish();
						fish.name = etName.getText().toString();
						if (genericEntityService.create(fish) != -1) {
							updateFishListView();
							dialog.dismiss();
						} else {
							Toast.makeText(v.getContext(), "Creation Failed",
									Toast.LENGTH_SHORT).show();
						}
					}
				});
				btnCancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
	}

	static class ViewHolder {
		protected TextView tvName;
		protected ImageView icon;
		protected int position;
		protected Fish fish;
		private int imageid;

		public ViewHolder() {
			position = 0;
			imageid = R.drawable.bullet_go;
		}

		public int getImageId() {
			return imageid;
		}
	}

	public class FishArrayAdapter extends ArrayAdapter<Fish> {
		private ArrayList<Fish> allModelItemsArray;
		private Activity context;
		private LayoutInflater inflator;
		private OnTouchListener listener;

		public FishArrayAdapter(Activity context, ArrayList<Fish> list,
				OnTouchListener _listener) {
			super(context, R.layout.fish_directory_row, list);
			this.listener = _listener;
			this.context = context;
			this.allModelItemsArray = new ArrayList<Fish>();

			this.allModelItemsArray.addAll(list);
			inflator = context.getLayoutInflater();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;
			if (position > allModelItemsArray.size())
				return null;
			Fish fish = allModelItemsArray.get(position);
			final ViewHolder viewHolder = new ViewHolder();
			ViewHolder Holder = null;
			if (convertView == null) {

				view = inflator.inflate(R.layout.fish_directory_row, null);

				view.setTag(viewHolder);

				viewHolder.tvName = (TextView) view.findViewById(R.id.tvName);
				viewHolder.icon = (ImageView) view.findViewById(R.id.icon);
				viewHolder.position = position;
				Holder = viewHolder;
			} else {
				view = convertView;
				Holder = ((ViewHolder) view.getTag());
			}
			if (this.listener != null) {
				view.setOnTouchListener(this.listener);
			}
			Holder.fish = fish;
			Holder.position = position;
			Holder.tvName.setText(fish.name);
			return view;
		}
	}
}
