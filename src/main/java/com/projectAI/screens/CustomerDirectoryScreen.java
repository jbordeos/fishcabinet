package com.projectAI.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projectAI.FishCabinetApplication;
import com.projectAI.R;
import com.projectAI.ancore.domain.BaseActivity;
import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.ancore.service.GenericListAdapter;
import com.projectAI.models.Customer;

public class CustomerDirectoryScreen extends BaseActivity {
	String D = "CUSTOMERDIRECTORY";
	public OnTouchListener gestureListener;
	private ListView lvCustomerDirectory;
	private Button btnAddCustomer;
	private final int[] colors = { Color.WHITE, Color.CYAN };
	final static int CONTACT_REQUEST_CODE = 10;
	private GenericEntityService genericEntityService;
	private ArrayList<Customer> customerList;
	private Map<String, Customer> custMap;
	private GenericListAdapter<Customer> adapter;

	private EditText dialogEtName;
	private EditText dialogEtContactNo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.customer_directory);
	}

	@Override
	protected void doInBackgroundProcess() {
		genericEntityService = ((FishCabinetApplication) getApplication())
				.getGenericEntityService();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onPostExecuteProcess() {
		initComponent();
	}

	private void deleteDialog(final View v, final int pos) {
		String name = ((TextView) v.findViewById(R.id.tvName)).getText()
				.toString();
		AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
		builder.setIcon(android.R.drawable.ic_delete)
				.setTitle("DELETE " + name)
				.setMessage("Are you sure you?")
				.setPositiveButton("DELETE",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								if (genericEntityService.delete(customerList
										.get(pos))) {
									customerList.remove(pos);
									updateCustomerListView();
								} else {
									Toast.makeText(v.getContext(), "Failed",
											Toast.LENGTH_LONG).show();
								}
							}
						}).setNegativeButton("CANCEL", null);
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void updateCustomerListView() {
		customerList = genericEntityService.findAll(Customer.class);
		if (customerList != null) {
			custMap = new HashMap<String, Customer>();
			for (Customer c : customerList) {
				custMap.put(c.contactNo, c);
			}

			gestureListener = new View.OnTouchListener() {
				private int padding = 0;
				private int initialx = 0;
				private int currentx = 0;
				private ViewHolder viewHolder;
				private int color = Color.parseColor("#ffffff");
				private int colorRed = Color.parseColor("#F9966B");
				private int colorWhite = Color.parseColor("#ffffff");
				private int img = android.R.drawable.presence_online;
				private int imgArrow = android.R.drawable.presence_online;
				private int imgWarn = android.R.drawable.presence_offline;

				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						Log.v(D, "Action DOWN");
						padding = 0;
						initialx = (int) event.getX();
						currentx = (int) event.getX();
						viewHolder = ((ViewHolder) v.getTag());
					}
					if (event.getAction() == MotionEvent.ACTION_MOVE) {
						currentx = (int) event.getX();
						padding = currentx - initialx;
						Log.v(D, "Action MOVE : padding = " + padding);
					}

					if (event.getAction() == MotionEvent.ACTION_UP
							|| event.getAction() == MotionEvent.ACTION_CANCEL) {
						Log.v(D, "Action UP or CANCEL");
						if (padding > 100) {
							deleteDialog(v, viewHolder.position);
						}
						padding = 0;
						initialx = 0;
						currentx = 0;
						color = colorWhite;
						img = imgArrow;
					}

					if (viewHolder != null) {
						if (padding >= 100) {
							Log.v(D, "Padding > 75");
							color = colorRed;
							img = imgWarn;
						}
						if (padding < 100) {
							color = colorWhite;
							img = imgArrow;
						}
						v.setBackgroundColor(color);
						viewHolder.icon.setImageResource(img);
						v.setPadding(padding, 0, 0, 0);
					}
					return true;
				}
			};

			CustomerArrayAdapter adapter = new CustomerArrayAdapter(this,
					customerList, gestureListener);
			lvCustomerDirectory.setAdapter(adapter);
		} else {
			CustomerArrayAdapter adapter = new CustomerArrayAdapter(this,
					new ArrayList<Customer>(), gestureListener);
			lvCustomerDirectory.setAdapter(adapter);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CONTACT_REQUEST_CODE) {
			if (data != null) {
				Uri contactData = data.getData();
				Cursor c = managedQuery(contactData, null, null, null, null);
				if (c.moveToFirst()) {
					String num = c
							.getString(
									c.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
							.toString();
					String name = c
							.getString(
									c.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
							.toString();
					dialogEtName.setText(name);
					dialogEtContactNo.setText(num.replaceAll("\\+63", "0"));

				}
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}


	private void initComponent() {
		lvCustomerDirectory = (ListView) findViewById(R.id.lvCustomerDirectory);
		updateCustomerListView();

		btnAddCustomer = (Button) findViewById(R.id.btnAddCustomer);
		btnAddCustomer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final Dialog dialog = new Dialog(v.getContext());
				dialog.setTitle("Add Customer");
				dialog.setContentView(R.layout.add_customer_dialog);
				dialogEtName = (EditText) dialog.findViewById(R.id.etName);
				final EditText dialogEtAddress = (EditText) dialog
						.findViewById(R.id.etAddress);
				dialogEtContactNo = (EditText) dialog
						.findViewById(R.id.etContactNo);
				Button btnBrowseContacts = (Button) dialog
						.findViewById(R.id.btnBrowseContacts);
				Button btnSave = (Button) dialog.findViewById(R.id.btnSave);
				Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
				btnBrowseContacts.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						startActivityForResult(
								new Intent(
										Intent.ACTION_PICK,
										ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
								CONTACT_REQUEST_CODE);
					}
				});
				btnSave.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						String number = ((EditText) dialog
								.findViewById(R.id.etContactNo)).getText()
								.toString();
						if (custMap != null && custMap.containsKey(number)) {
							Toast.makeText(
									v.getContext(),
									"Contact Already exists: ("
											+ custMap.get(number).contactNo
											+ ")", Toast.LENGTH_SHORT).show();
						} else {
							Customer c = new Customer();
							c.name = ((EditText) dialog
									.findViewById(R.id.etName)).getText()
									.toString();
							c.address = dialogEtAddress.getText().toString();
							c.contactNo = number;
							if (genericEntityService.create(c) != -1) {
								updateCustomerListView();
								dialog.dismiss();
							} else {
								Toast.makeText(v.getContext(),
										"Creation Failed", Toast.LENGTH_SHORT)
										.show();
							}
						}
					}
				});
				btnCancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
	}

	static class ViewHolder {
		protected TextView tvName;
		protected TextView tvContactNo;
		protected ImageView icon;
		protected int position;
		protected Customer customer;
		private int imageid;

		public ViewHolder() {
			position = 0;
			imageid = R.drawable.bullet_go;
		}

		public int getImageId() {
			return imageid;
		}
	}

	public class CustomerArrayAdapter extends ArrayAdapter<Customer> {
		private ArrayList<Customer> allModelItemsArray;
		private Activity context;
		private LayoutInflater inflator;
		private OnTouchListener listener;

		public CustomerArrayAdapter(Activity context, ArrayList<Customer> list,
				OnTouchListener _listener) {
			super(context, R.layout.customer_directory_row, list);
			this.listener = _listener;
			this.context = context;
			this.allModelItemsArray = new ArrayList<Customer>();

			this.allModelItemsArray.addAll(list);
			inflator = context.getLayoutInflater();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;
			if (position > allModelItemsArray.size())
				return null;
			Customer c = allModelItemsArray.get(position);
			final ViewHolder viewHolder = new ViewHolder();
			ViewHolder Holder = null;
			if (convertView == null) {

				view = inflator.inflate(R.layout.customer_directory_row, null);

				view.setTag(viewHolder);

				viewHolder.tvName = (TextView) view.findViewById(R.id.tvName);
				viewHolder.tvContactNo = (TextView) view
						.findViewById(R.id.tvContactNo);
				viewHolder.icon = (ImageView) view.findViewById(R.id.icon);
				viewHolder.position = position;
				Holder = viewHolder;
			} else {
				view = convertView;
				Holder = ((ViewHolder) view.getTag());
			}

			if (this.listener != null) {
				view.setOnTouchListener(this.listener);
				Log.v(D, " not null listener" + this.listener);
			} else {
				Log.v(D, " null listener" + this.listener);
			}

			Holder.customer = c;
			Holder.position = position;
			Holder.tvName.setText(c.name);
			Holder.tvContactNo.setText(c.contactNo);
			return view;
		}
	}
}
