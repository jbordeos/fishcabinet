package com.projectAI.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.projectAI.R;
import com.projectAI.models.Model;

public class TestSwipeScreen  extends Activity {
    String D = "TestSwipe";
    public OnTouchListener gestureListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ListView lv = (ListView)findViewById(R.id.mylistview);
        
        gestureListener = new View.OnTouchListener() {
            private int padding = 0;
            private int initialx = 0;
            private int currentx = 0;
            private  ViewHolder viewHolder;
            public boolean onTouch(View v, MotionEvent event) {
                if ( event.getAction() == MotionEvent.ACTION_DOWN)
                {
                	Log.v(D, "Action DOWN");
                    padding = 0;
                    initialx = (int) event.getX();
                    currentx = (int) event.getX();
                    viewHolder = ((ViewHolder) v.getTag());
                }
                if ( event.getAction() == MotionEvent.ACTION_MOVE)
                {
                    currentx = (int) event.getX();
                    padding = currentx - initialx;
                    Log.v(D, "Action MOVE : padding = " + padding);
                }
                
                if ( event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                {
                	Log.v(D, "Action UP or CANCEL");
                    padding = 0;
                    initialx = 0;
                    currentx = 0;
                }
                
                if(viewHolder != null)
                {
                    if(padding == 0)
                    {
                    	Log.v(D, "Padding 0");
                        v.setBackgroundColor(0xFF000000 );
                    }
                    if(padding > 75)
                    {
                    	Log.v(D, "Padding > 75");
                        viewHolder.setRunning(true);
                    }
                    if(padding < -75)
                    {
                    	Log.v(D, "Padding < -75");
                        viewHolder.setRunning(false);
                    }
                    v.setBackgroundColor(viewHolder.getColor());  
                    viewHolder.icon.setImageResource(viewHolder.getImageId());
                    v.setPadding(padding, 0,0, 0);
                }
                return true;
            }
        };
        
        ModelArrayAdapter adapter = new ModelArrayAdapter(this, getData(),gestureListener);
        lv.setAdapter(adapter);
    }
    
    public ArrayList<Model> getData()
    {
        ArrayList<Model> models = new ArrayList<Model>();
        for(int a=0;a<10;a++)
        {
            Model m = new Model(String.format("Item %d", a));
            models.add(m);
        }
        return models;
    }

    static class ViewHolder {
        protected TextView text;
        protected ImageView icon;
        protected CheckBox checkbox;
        protected int position;
        protected Model model;
        private int color;
        private int imageid;
        
        public ViewHolder()
        {
            position = 0;
            imageid = R.drawable.bullet_go;
            color = 0xFFFFFFFF;
        }
        public int getColor() {
            return color;
        }
        public int getImageId() {
            return imageid;
        }
        public void setRunning(boolean running) {
            model.setRuning(running);
            if(running)
            {
                color = 0xFFffffb6;
                //yellow
                imageid = R.drawable.bullet_green;
            }
            else
            {
                imageid = R.drawable.bullet_go;
                // white
                color = 0xFFFFFFFF;
            }
        }
    }
    
    public class ModelArrayAdapter extends ArrayAdapter<Model>
    {
        private ArrayList<Model> allModelItemsArray;
        private Activity context;
        private LayoutInflater inflator;
        private OnTouchListener listener;
        
        public ModelArrayAdapter(Activity context, ArrayList<Model> list,OnTouchListener _listener) {
            super(context, R.layout.singlerow, list);
            this.listener = _listener;
            this.context = context;
            this.allModelItemsArray = new ArrayList<Model>();

            this.allModelItemsArray.addAll(list);
            inflator = context.getLayoutInflater();
        }
        
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = null;
            if(position > allModelItemsArray.size())
                return null;
            Model m = allModelItemsArray.get(position);
            final ViewHolder viewHolder = new ViewHolder();
            ViewHolder Holder = null;
            if (convertView == null) {
                
                view = inflator.inflate(R.layout.singlerow, null);
                
                view.setTag(viewHolder);
                
                viewHolder.text = (TextView) view.findViewById(R.id.label);
                viewHolder.checkbox = (CheckBox) view.findViewById(R.id.check);
                viewHolder.icon = (ImageView) view.findViewById(R.id.icon);
                viewHolder.checkbox.setTag(m);
                viewHolder.position = position;
                
                Holder = viewHolder;
            } else {
                view = convertView;
                Holder = ((ViewHolder) view.getTag());
            }
            
            if(this.listener != null)
                view.setOnTouchListener(this.listener);

            Holder.model = m;
            Holder.position = position;
            Holder.text.setText(m.getName());
            return view;
        }
    }
}