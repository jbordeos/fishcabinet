package com.projectAI.screens;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.projectAI.FishCabinetApplication;
import com.projectAI.R;
import com.projectAI.ancore.domain.BaseActivity;
import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.helper.DateHelper;
import com.projectAI.models.Fish;
import com.projectAI.models.TodaysFish;

public class TodaysFishCabinetScreen extends BaseActivity {

	private Button btnSave;
	private TextView tvDate;
	private ListView lvList;
	private Date date;
	private GenericEntityService genericEntityService;
	private ArrayList<FishModel> list;
	private DateHelper dateHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.todays_fish_cabinet);
	}

	private void initComponent() {
		tvDate = (TextView) findViewById(R.id.tvDate);
		date = Calendar.getInstance().getTime();
		tvDate.setText(dateHelper.formatToDisplayDate(date));
		lvList = (ListView) findViewById(R.id.lvList);

		OnClickListener listener = new OnClickListener() {
			private ViewHolder viewHolder;

			@Override
			public void onClick(View v) {
				viewHolder = (ViewHolder) v.getTag();
				FishModel fm = (FishModel) viewHolder.checkbox.getTag();
				if (fm.selected) {
					fm.selected = !fm.selected;
					viewHolder.checkbox.setChecked(fm.selected);
					list.get(viewHolder.position).selected = fm.selected;
				} else {
					callDialog(v, viewHolder);
				}
			}
		};
		ArrayAdapter<FishModel> adapter = new InteractiveArrayAdapter(this,
				list, listener);
		lvList.setAdapter(adapter);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new SaveTodaysFishAsync().execute();
			}
		});
	}

	private class SaveTodaysFishAsync extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			showProgressDialog("Saving...", getContext());
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			saveTodaysFish();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			try {
				dismissProgressDialog();
				finish();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void saveTodaysFish() {
		String strDate = dateHelper.formatToDBDate(date);
		String query = "delete from " + TodaysFish.class.getSimpleName()
				+ " where date = '" + strDate + "'";
		genericEntityService.sampleCustomQuery(query, TodaysFish.class);
		TodaysFish tf = null;
		for (FishModel fm : list) {
			if (fm.selected) {
				tf = new TodaysFish(strDate, fm.fish.id, fm.fish.name,
						fm.fish.bPrice, fm.fish.sPrice, fm.fish.kilo);
				genericEntityService.create(tf);
			}
		}
	}

	private void displayList() {
		StringBuilder sb = new StringBuilder();
		for (FishModel fm : list) {
			sb.append(fm.fish.name + " : " + fm.selected + "\n");
		}
		Log.d("list bool", sb.toString());
	}

	private void callDialog(final View v, final ViewHolder viewHolder) {
		final ViewHolder vh = (ViewHolder) v.getTag();
		final int pos = vh.position;
		final FishModel fm = (FishModel) vh.checkbox.getTag();
		Log.d("call Dialog", "name: " + fm.fish.name);
		Log.d("call Dialog", "position: " + vh.position);
		Log.d("call Dialog", "pos: " + pos);
		displayList();
		final Dialog dialog = new Dialog(v.getContext());
		dialog.setTitle("Set Fish Info For Today");
		dialog.setContentView(R.layout.todays_fish_cabinet_dialog);
		/*final EditText etKilo = (EditText) dialog.findViewById(R.id.etKilo);
		final EditText etBuyPrice = (EditText) dialog
				.findViewById(R.id.etBuyPrice);*/
		final EditText etSellPrice = (EditText) dialog
				.findViewById(R.id.etSellPrice);
		Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
		btnOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Log.d("onClick Dialog", "name: " + fm.fish.name);
				Log.d("onClick Dialog", "position: " + vh.position);
				Log.d("onClick Dialog", "pos: " + pos);
				if (!etSellPrice.getText().toString().equals("")) {
					fm.selected = !fm.selected;
					vh.checkbox.setChecked(fm.selected);
					list.get(pos).selected = fm.selected;
					/*list.get(pos).fish.bPrice = Double.parseDouble(etBuyPrice
							.getText().toString());
					list.get(pos).fish.kilo = Double.parseDouble(etKilo.getText()
							.toString());*/
					list.get(pos).fish.sPrice = Double.parseDouble(etSellPrice
						.getText().toString());
					displayList();
					dialog.dismiss();
				} else {
					Toast.makeText(view.getContext(), "Please enter sell price", Toast.LENGTH_SHORT).show();
				}
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	static class ViewHolder {
		protected TextView text;
		protected CheckBox checkbox;
		protected int position;
	}

	public class InteractiveArrayAdapter extends ArrayAdapter<FishModel> {

		private final List<FishModel> list;
		private final Activity context;
		private OnClickListener listener;

		public InteractiveArrayAdapter(Activity context, List<FishModel> list,
				OnClickListener listener) {
			super(context, R.layout.todays_fish_cabinet_row, list);
			this.context = context;
			this.listener = listener;
			this.list = list;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = null;
			if (convertView == null) {
				LayoutInflater inflator = context.getLayoutInflater();
				view = inflator.inflate(R.layout.todays_fish_cabinet_row, null);
				final ViewHolder viewHolder = new ViewHolder();
				viewHolder.text = (TextView) view.findViewById(R.id.label);
				viewHolder.checkbox = (CheckBox) view.findViewById(R.id.check);
				viewHolder.position = position;
				view.setTag(viewHolder);
				viewHolder.checkbox.setTag(list.get(position));
			} else {
				view = convertView;
				((ViewHolder) view.getTag()).checkbox
						.setTag(list.get(position));
				((ViewHolder) view.getTag()).position = position;
			}
			if (this.listener != null) {
				view.setOnClickListener(this.listener);
			}
			Log.d("getView>>>", "position: " + position);
			ViewHolder holder = (ViewHolder) view.getTag();
			holder.text.setText(list.get(position).fish.name);
			holder.checkbox.setChecked(list.get(position).selected);
			return view;
		}
	}

	@Override
	protected void doInBackgroundProcess() {
		dateHelper = new DateHelper();
		genericEntityService = ((FishCabinetApplication) getApplication())
				.getGenericEntityService();
		/*
		 * for (int i = 0; i < 3; i++) { Fish f = new Fish(); f.name = (i + 1) +
		 * ""; genericEntityService.create(f); }
		 */
		String query = "select * from " + TodaysFish.class.getSimpleName()
				+ " where date = '"
				+ dateHelper.formatToDBDate(dateHelper.getCurrentDate()) + "'";
		ArrayList<TodaysFish> tfList = genericEntityService.sampleCustomQuery(
				query, TodaysFish.class);
		String query2 = "select * from Fish order by name asc";
		ArrayList<Fish> flist = genericEntityService.sampleCustomQuery(query2, Fish.class); 
		list = new ArrayList<FishModel>();
		for (int i = 0; i < flist.size(); i++) {
			if (tfList != null) {
				boolean exist = false;
				for (int x = 0; x < tfList.size(); x++) {
					if (tfList.get(x).fishId == flist.get(i).id) {
						Fish fish = new Fish();
						fish.id = tfList.get(x).fishId;
						fish.name = tfList.get(x).fishName;
						fish.bPrice = tfList.get(x).bPrice;
						fish.sPrice = tfList.get(x).sPrice;
						fish.kilo = tfList.get(x).kilo;
						FishModel fm = new FishModel(fish);
						fm.selected = true;
						list.add(fm);
						exist = true;
						Log.d("exist", i + "");
						break;
					}
				}
				if (!exist) {
					list.add(new FishModel(flist.get(i)));
					Log.d("not exist", i + "");
				}
			} else {
				list.add(new FishModel(flist.get(i)));
				Log.d("not exist", i + "");
			}
		}
	}

	@Override
	protected void onPostExecuteProcess() {
		initComponent();
	}
	
	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				getContext());
		builder.setTitle("Close w/o saving?").setPositiveButton("YES", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				finish();
			}
		}).setNegativeButton("NO", null);
		AlertDialog alert = builder.create();
		alert.show();
	}

	public class FishModel {
		private Fish fish;
		private boolean selected;

		public FishModel(Fish fish) {
			this.fish = fish;
			selected = false;
		}
	}
}
