package com.projectAI.screens;

import java.util.ArrayList;
import java.util.Date;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.projectAI.FishCabinetApplication;
import com.projectAI.R;
import com.projectAI.ancore.domain.BaseActivity;
import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.helper.DateHelper;
import com.projectAI.models.Customer;
import com.projectAI.models.Fish;
import com.projectAI.models.SentItem;
import com.projectAI.models.TodaysFish;

public class DashboardScreen extends BaseActivity implements OnClickListener {

	private Button btnFishDirectory;
	private Button btnSendTodaysFish;
	private Button btnInbox;
	private Button btnSentItems;
	private Button btnTFC;
	private Button btnCustomers;
	private static final String TAG = "DashboardScreen";
	private ListView llFish;
	private GenericEntityService genericEntityService;
	private ArrayList<TodaysFish> fishList;
	private ArrayList<String> fishNames;
	private DateHelper dateHelper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
	}

	@Override
	public void doInBackgroundProcess() {
		initData();
		initSetupData();
	}

	private void initSetupData() {
		if (genericEntityService.findAll(Fish.class) == null) {
			Fish f;
			String fishes = "Alamang,Alumahan,Bacoco,Bangus,Belly ng Blue Marlin,Belly ng Malasogi,Belly ng Tanigui,Belly ng Tuna,Blue Marlin,Cream Douri,Dolong,G-G,Gindara,Golden Fish,Harosep,Hasa,Iwas,Labahita,Lapu-lapu,Lumot,Malasogui,Manok,Matambaka,Maya-maya,Pusit,Sap-sap,Suahe,Suaheng Buhay,Sugpo,Tahong,Talakitok,Talakitok sliced,Tanigui sliced,Tilapia,Tuna sliced,Ulo ng Pink Salmon,Ulo ng Talakitok,Ulo ng Tanigui,Ulo ng Tuna,Yellow Fin Tuna,Buntot ng Tuna,Torsilyo";
			String[] fishesArray = fishes.split(",");
			for (int x = 0; x < fishesArray.length; x++) {
				f = new Fish();
				f.name = fishesArray[x];
				genericEntityService.create(f);
			}
		}
	}
	
	private void initData() {
		dateHelper = new DateHelper();
		genericEntityService = ((FishCabinetApplication) getApplication())
				.getGenericEntityService();
		String query = "select * from " + TodaysFish.class.getSimpleName()
				+ " where date = '"
				+ dateHelper.formatToDBDate(dateHelper.getCurrentDate()) + "'" +
						" order by fishName asc";
		fishList = genericEntityService.sampleCustomQuery(query,
				TodaysFish.class);
		fishNames = new ArrayList<String>();
		if (fishList != null) {
			for (int i = 0; i < fishList.size(); i++) {
				fishNames.add(fishList.get(i).fishName + " : P"
						+ fishList.get(i).sPrice + "");
			}
		} else {
			fishNames.add("No fishes set today");
		}
	}

	@Override
	public void onPostExecuteProcess() {
		initComponent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnFishDirectory:
			startActivity(new Intent(this, FishDirectoryScreen.class));
			break;
		case R.id.btnInbox:
			startActivity(new Intent(this, InboxScreen.class));
			break;
		case R.id.btnCustomers:
			startActivity(new Intent(this, CustomerDirectoryScreen.class));
			break;
		case R.id.btnTodaysFishCabinet:
			startActivity(new Intent(this, TodaysFishCabinetScreen.class));
			/*
			 * Annotation[] a = null; try { a =
			 * Fish.class.getField("name").getAnnotations(); } catch
			 * (SecurityException e) { e.printStackTrace(); } catch
			 * (NoSuchFieldException e) { e.printStackTrace(); } StringBuilder
			 * sb = new StringBuilder(); for(Annotation an: a) {
			 * sb.append(((SampleAnnotation)an).fieldType()); }
			 * Toast.makeText(this, a.length + " - "+ sb.toString(),
			 * Toast.LENGTH_LONG).show();
			 */
			break;
		case R.id.btnSendTodaysFish:
			startActivity(new Intent(this, CustomerSelectScreen.class));
			break;
		case R.id.btnSentItems:
			startActivity(new Intent(this, SentItemsScreen.class));
			break;
		default:
			break;
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	protected void onResume() {
		initData();
		initComponent();
		updateFishListView();
		super.onResume();
	}

	private void logSentItems() {
		ArrayList<SentItem> sis = genericEntityService.findAll(SentItem.class);
		for (SentItem si : sis) {
			Log.d("FISH CABINET", si.name + "\n" + si.message + "\n" + si.date
					+ "\n" + si.sent + "\n\n");
		}
	}

	private void updateFishListView() {
		Log.d(TAG, "update fish list");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.dashboard_fish_row, fishNames);
		llFish.setAdapter(adapter);
	}

	private void initComponent() {
		llFish = (ListView) findViewById(R.id.lvFish);
		updateFishListView();
		btnTFC = (Button) findViewById(R.id.btnTodaysFishCabinet);
		btnTFC.setOnClickListener(this);
		btnFishDirectory = (Button) findViewById(R.id.btnFishDirectory);
		btnFishDirectory.setOnClickListener(this);
		btnInbox = (Button) findViewById(R.id.btnInbox);
		btnInbox.setOnClickListener(this);
		btnSentItems = (Button) findViewById(R.id.btnSentItems);
		btnSentItems.setOnClickListener(this);
		btnSendTodaysFish = (Button) findViewById(R.id.btnSendTodaysFish);
		btnSendTodaysFish.setOnClickListener(this);
		btnCustomers = (Button) findViewById(R.id.btnCustomers);
		btnCustomers.setOnClickListener(this);
	}

}
