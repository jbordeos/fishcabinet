package com.projectAI.screens;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.projectAI.FishCabinetApplication;
import com.projectAI.R;
import com.projectAI.ancore.domain.BaseActivity;
import com.projectAI.ancore.service.GenericEntityService;
import com.projectAI.helper.DateHelper;
import com.projectAI.models.Customer;
import com.projectAI.models.SentItem;

public class SentItemsScreen extends BaseActivity implements
		OnItemClickListener {

	private DateHelper dateHelper;
	private ListView lvSentItems;
	private ArrayList<SentItem> sentList;
	private ArrayList<HashMap<String, String>> sentMap;
	private GenericEntityService genericEntityService;
	private boolean alreadySend = false;
	private boolean sentItemFlag = false;
	private ArrayList<Customer> custList;
	private Map<String, Customer> custMap;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sent_items);
	}

	private void initComponent() {
		lvSentItems = (ListView) findViewById(R.id.lvSentItems);
		updateSentList();
	}

	private void updateSentList() {
		sentMap = new ArrayList<HashMap<String, String>>();
		String query = "select * from SentItem order by date desc";
		sentList = genericEntityService
				.sampleCustomQuery(query, SentItem.class);
		if (sentList != null && sentList.size() != 0) {
			for (int i = 0; i < sentList.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("id", sentList.get(i).id.toString());
				map.put("sender", sentList.get(i).name.toString());
				map.put("senderNo", sentList.get(i).number.toString());
				map.put("date", sentList.get(i).date.toString());
				map.put("status", sentList.get(i).sent + "");
				map.put("body", sentList.get(i).message.toString());
				sentMap.add(map);
			}
		}
		if (sentMap != null && sentMap.size() != 0) {
			String[] from = new String[] { "sender", "date", "status", "body" };
			int[] to = new int[] { R.id.tvSender, R.id.tvDate, R.id.tvStatus,
					R.id.tvBody };
			SimpleAdapter adapter = new SimpleAdapter(this, sentMap,
					R.layout.sent_items_row, from, to);
			lvSentItems.setAdapter(adapter);
			lvSentItems.setOnItemClickListener(this);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int index, long arg3) {
		Log.d("SENTSCREEN",
				custMap.size() + " : " + sentMap.get(index).get("senderNo"));
		Customer customer = custMap.get(sentMap.get(index).get("senderNo"));
		callReplyDialog(sentMap.get(index).get("body"), customer, index);
	}

	private void callReplyDialog(String msg, final Customer cust,
			final int index) {
		final Dialog dialog = new Dialog(getContext());
		dialog.setTitle("Resend to " + cust.name + " (" + cust.contactNo + ")");
		dialog.setContentView(R.layout.reply_dialog);
		final EditText etMsg = (EditText) dialog.findViewById(R.id.etMsg);
		etMsg.setText(msg);
		Button btnSend = (Button) dialog.findViewById(R.id.btnSend);
		Button btnClear = (Button) dialog.findViewById(R.id.btnClear);
		Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
		btnClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				etMsg.setText("");
			}
		});
		btnSend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (etMsg.getText().toString().length() > 0) {
					SmsManager sms = SmsManager.getDefault();
					ArrayList<String> parts = sms.divideMessage(etMsg.getText()
							.toString());
					for (int i = 0; i < parts.size(); i++) {
						sendSMS(cust, etMsg.getText().toString(), index,
								"(" + (i + 1) + "/" + parts.size() + ") \n"
										+ parts.get(i));
					}
					dialog.dismiss();
				} else {
					Toast.makeText(v.getContext(), "Please enter your message",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	@Override
	protected void doInBackgroundProcess() {
		dateHelper = new DateHelper();
		genericEntityService = ((FishCabinetApplication) getApplication())
				.getGenericEntityService();
		custList = genericEntityService.findAll(Customer.class);
		if (custList != null) {
			custMap = new HashMap<String, Customer>();
			for (int i = 0; i < custList.size(); i++) {
				custMap.put(custList.get(i).contactNo, custList.get(i));
			}
		}
	}

	@Override
	protected void onPostExecuteProcess() {
		initComponent();
	}

	private void updateSentItem(Customer customer, String message,
			boolean sent, int index) {
		SentItem si = new SentItem(customer.name, customer.contactNo, message,
				new Date().toString(), sent);
		si.id = Long.parseLong(sentMap.get(index).get("id"));
		si.sent = true;
		genericEntityService.update(si);
	}

	private void sendSMS(final Customer cust, final String message,
			final int index, final String messagePart) {
		showProgressDialog("Please wait...", this);
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";

		final PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
				new Intent(SENT), PendingIntent.FLAG_UPDATE_CURRENT);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 1,
				new Intent(DELIVERED), 0);
		if (!alreadySend) {
			// ---when the SMS has been sent---
			registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent intent) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS Sent",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS Sent");
						if (!sentItemFlag) {
							updateSentItem(cust, message, true, index);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						AlertDialog.Builder builder = new AlertDialog.Builder(
								getContext());
						builder.setTitle("You have no load").setNegativeButton(
								"OK", null);
						AlertDialog alert = builder.create();
						alert.show();
						if (!sentItemFlag) {
							updateSentItem(cust, message, false, index);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						Toast.makeText(getBaseContext(), "No service",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							updateSentItem(cust, message, false, index);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						Toast.makeText(getBaseContext(), "Null PDU",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							updateSentItem(cust, message, false, index);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						Toast.makeText(getBaseContext(), "Radio off",
								Toast.LENGTH_SHORT).show();
						if (!sentItemFlag) {
							updateSentItem(cust, message, false, index);
							sentItemFlag = true;
						}
						try {
							dismissProgressDialog();
						} catch (Exception e) {
							e.printStackTrace();
						}
						break;
					}
				}
			}, new IntentFilter(SENT));

			// ---when the SMS has been delivered---
			registerReceiver(new BroadcastReceiver() {
				@Override
				public void onReceive(Context arg0, Intent arg1) {
					switch (getResultCode()) {
					case Activity.RESULT_OK:
						Toast.makeText(getBaseContext(), "SMS delivered",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS delivered");
						break;
					case Activity.RESULT_CANCELED:
						Toast.makeText(getBaseContext(), "SMS not delivered",
								Toast.LENGTH_SHORT).show();
						Log.d("INBOXSCREEN", "SMS not delivered");
						break;
					}
				}
			}, new IntentFilter(DELIVERED));
		}

		SmsManager sms = SmsManager.getDefault();
		ArrayList<String> parts = sms.divideMessage(messagePart);
		try {
			sms.sendTextMessage(cust.contactNo, null, parts.get(0), sentPI,
					deliveredPI);
			Log.i("AIiiiiiiiiiiiiiiiiiiiiii", cust.contactNo + "\n" + message
					+ "\n" + sentPI + "\n" + deliveredPI);
			alreadySend = true;
			updateSentList();
		} catch (Exception e) {
			Toast.makeText(this, "error" + e.getMessage(), Toast.LENGTH_LONG)
					.show();
		}
	}
}