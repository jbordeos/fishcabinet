package com.projectAI;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ReceiverActivity extends Activity implements OnClickListener {
    TextView tvEncMsg;
    TextView tvDecMsg;
    Button btnDec;
    String encMsg;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reciever);
        tvEncMsg = (TextView) findViewById(R.id.tvEnMsg);
        tvDecMsg = (TextView) findViewById(R.id.tvDeMsg);
        btnDec = (Button) findViewById(R.id.btnDecrypt);
        btnDec.setOnClickListener(this);
        encMsg = getIntent().getExtras().getString("msg");
        tvEncMsg.setText(encMsg);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDecrypt:
                String decMsg = "";
                try {
                    decMsg = StringCryptor.decrypt(encMsg, "salt");
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                tvDecMsg.setText(decMsg);
                break;
        }
    }
}