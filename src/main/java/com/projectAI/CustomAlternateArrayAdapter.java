package com.projectAI;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class CustomAlternateArrayAdapter extends ArrayAdapter<String> {

	public CustomAlternateArrayAdapter(Context context, int resource,
			int textViewResourceId, String[] objects) {
		super(context, resource, textViewResourceId, objects);
	}

	private int[] colors = new int[] { Color.CYAN,
			Color.BLUE };

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);

		int colorPos = position % colors.length;
		 view.setBackgroundColor(colors[colorPos]);
		/*view.setBackgroundDrawable(view.getResources().getDrawable(
				colors[colorPos]));*/
		return view;
	}

}
