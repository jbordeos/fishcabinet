package com.projectAI;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SenderScreen extends Activity implements OnClickListener {

	Button btnSend;
    Button btnBrowse;
    Button btnContacts;
    EditText etMsg;
    EditText etPN;
    TextView tvCount;
    final static int CONTACT_REQUEST_CODE = 10;
    private static String TAG = "FishCabinet";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		Log.i(TAG, "onCreate");
        setContentView(R.layout.sender);
        
        btnSend = (Button) findViewById(R.id.btnSend);
        etMsg = (EditText) findViewById(R.id.etMsg);
        etPN = (EditText) findViewById(R.id.etPNumber);
        btnBrowse = (Button) findViewById(R.id.btnBrowse);
        btnContacts = (Button) findViewById(R.id.btnContacts);
        btnSend.setOnClickListener(this);
        btnBrowse.setOnClickListener(this);
        btnContacts.setOnClickListener(this);
        tvCount = (TextView) findViewById(R.id.tvCount);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONTACT_REQUEST_CODE) {
            if (data != null) {
                Uri contactData = data.getData();
                Cursor c = managedQuery(contactData, null, null, null, null);
                if (c.moveToFirst()) {
                    etPN.setText(c.getString(c.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER)) + "");
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnContacts:
                startActivityForResult(new Intent(Intent.ACTION_PICK,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI),
                        CONTACT_REQUEST_CODE);
                break;
            case R.id.btnSend:
                String phoneNo = etPN.getText().toString();
                String message = etMsg.getText().toString();
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                    String salt = sharedPreferences.getString("salt", "");
                    try {
                        message = StringCryptor.encrypt(message, salt);
                    } catch (Exception e) {
                        Toast.makeText(this, "error encryption", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                if (phoneNo.length() > 0 && message.length() > 0)
                    sendSMS(phoneNo, message);
                else
                    Toast.makeText(getBaseContext(),
                            "Please enter both phone number and message.",
                            Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(this, RecieverActivity.class);
                intent.putExtra("msg", message);
                startActivity(intent);*/

                break;
            case R.id.btnBrowse:
                startActivity(new Intent(this, BrowseSmsActivity.class));
                break;
        }
    }

    private void sendSMS(String phoneNumber, String message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS ",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        try {
            sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        } catch (Exception e) {
            Toast.makeText(this, "error" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

}

